import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'


@Injectable()
export class RequestService {

    constructor(private _http: Http) { }

    // Function to make GET Requests
    getRequest(url){
      console.log(url);
      return this._http.get(url)
          .map(response => {
              { return response.json()};
          })
    }
}
