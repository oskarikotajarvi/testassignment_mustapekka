import { Component, OnInit, Input } from '@angular/core';
import { DriverService } from '../driver.service';
import { Observable } from 'rxjs';
import { NgModule } from '@angular/core';
import { Driver } from './../driver/driver.model';
import { RequestService } from '../requestService';
import {DriverListModule} from './driver-list.module';
import {AppRoutingModule} from '../app-routing.module'

@Component({
  selector: 'app-driver-list',
  templateUrl: './driver-list.component.html',
  styleUrls: ['./driver-list.component.css'],
  providers: [DriverService, RequestService]
})

export class DriverListComponent implements OnInit {

	result: string[];
    isLoading: boolean = true;
    drivers: any;
    sub: any;
    filter: string;
    title: string;
    years = [];
    selectedYear: string;
    currentYear = "2017";

  constructor( private _sharedService: DriverService) {
   }

  ngOnInit() {
    this.years=[
        {Year: "1950"}, {Year: "1951"},
        {Year: "1953"},{Year: "1954"}, {Year: "1955"}, {Year: "1956"}, {Year: "1957"}, {Year: "1958"}, {Year: "1959"}, {Year: "1960"}, {Year: "1961"}, {Year: "1962"}, {Year: "1963"},
        {Year: "1964"}, {Year: "1965"}, {Year: "1966"}, {Year: "1967"}, {Year: "1968"}, {Year: "1969"}, {Year: "1970"}, {Year: "1971"}, {Year: "1972"}, {Year: "1973"}, {Year: "1974"},
        {Year: "1975"}, {Year: "1976"}, {Year: "1977"}, {Year: "1978"}, {Year: "1979"}, {Year: "1980"}, {Year: "1981"}, {Year: "1982"}, {Year: "1983"}, {Year: "1984"}, {Year: "1985"},
        {Year: "1986"}, {Year: "1987"}, {Year: "1988"}, {Year: "1989"}, {Year: "1990"}, {Year: "1991"}, {Year: "1992"}, {Year: "1993"}, {Year: "1994"}, {Year: "1995"}, {Year: "1996"},
        {Year: "1997"}, {Year: "1998"}, {Year: "1999"}, {Year: "2000"}, {Year: "2001"}, {Year: "2002"}, {Year: "2003"}, {Year: "2004"}, {Year: "2005"}, {Year: "2006"}, {Year: "2007"},
        {Year: "2008"}, {Year: "2009"}, {Year: "2010"}, {Year: "2011"}, {Year: "2012"}, {Year: "2013"}, {Year: "2014"}, {Year: "2015"}, {Year: "2016"}, {Year: "2017"}, {Year: "2018"}
    ];
    this.getLastYearDrivers();
    this.selectedYear = this.years[0];

  }

  getLastYearDrivers(){
      this.setIsLoadingProgress();
      this._sharedService.getSelectYearDrivers(2017)
        .subscribe(
            lstresult => {
                this.addDataAboutDrivers(lstresult);
            },
            error => {
                console.log("Error. The JSON value is as follows:");
                console.log(error);
                this.isLoadingFinish();
            }
        );
  }

  setIsLoadingProgress(){
      this.isLoading = true;
  }

  isLoadingFinish(){
      this.isLoading = false;
  }

  addDataAboutDrivers(lstresult){
      this.drivers = lstresult["MRData"]["DriverTable"]["Drivers"];
      this.isLoadingFinish();
  }

  onYearSelect(val: string){
      this.onSelect(val);
  }

  onSelect(val:string){
      this.setIsLoadingProgress();
      this._sharedService.getSelectYearDrivers(val)
        .subscribe(
            lstresult => {
                this.addDataAboutDrivers(lstresult);
            },
            error => {
                console.log("Error. The JSON value is as follows:");
                console.log(error);
                this.isLoadingFinish();
            }
        )
  }

  updateCurrentYear(val: string){
      this.currentYear = val;
  }


}
