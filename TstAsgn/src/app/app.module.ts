import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { DriverService } from './driver.service';
import { HttpClientModule } from '@angular/common/http';
import { DriverListModule } from './driver-list/driver-list.module';
import { DriverModule } from './driver/driver.module';
import { HttpModule } from '@angular/http';
import { RequestService } from './requestService'
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    DriverModule,
    DriverListModule,
    AppRoutingModule,
    HttpModule,
    FormsModule

  ],
  providers: [DriverService],
  bootstrap: [AppComponent]
})
export class AppModule { }
