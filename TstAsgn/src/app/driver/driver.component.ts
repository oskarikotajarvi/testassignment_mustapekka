import { Component, OnInit, Input, Directive } from '@angular/core';
import { DriverService } from '../driver.service';
import { ActivatedRoute } from '@angular/router';
import { Driver } from './driver.model';
import { RequestService } from '../requestService'

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.css'],
  providers: [RequestService]
})
export class DriverComponent implements OnInit {

    driverId: string;
    driver: any;
    isLoading: boolean = true;
    private sub:any;
    name: string;
    dateOfBirth: string;
    permanentNumber: number;
    nationality: string;
    url: string;

  constructor( private _sharedService: DriverService, private route: ActivatedRoute ) { }

  ngOnInit() {
    // Make service call for single driver here.
    this.sub = this.route.params.subscribe(params => {
        this.driverId = params['id'];
        this.getLastYearDrivers();
        })
  }

  getLastYearDrivers(){
      this.setIsLoadingProgress();
      this._sharedService.show(this.driverId)
        .subscribe(
            lstresult => {
                console.log("loading data");
                this.driver = lstresult["MRData"]["DriverTable"]["Drivers"][0];
                console.log("this.driver.nationality");
                this.name = this.givenName + " " + this.familyName;
                this.nationality = this.driver.nationality;
                this.url = this.driver.url;
                this.dateOfBirth = this.driver.dateOfBirth;
                this.permanentNumber = this.driver.permanentNumber;
                this.isLoadingFinish();
            },
            error => {
                console.log("Error. The result JSON value is as follows:");
                console.log(error);
                this.isLoadingFinish();
            }
        )
  }

  setIsLoadingProgress(){
      this.isLoading = true;
  }

  isLoadingFinish(){
      this.isLoading = false;
  }

}
