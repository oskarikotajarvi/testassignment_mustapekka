import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { NgModule } from '@angular/core';
import { RequestService } from './requestService';

import {Observable} from 'rxjs';
import {URL_LOCALHOST, DRIVERS_URLS} from './URLS/urls';

@Injectable()
export class DriverService {
  // Use the api documentation from
  // http://ergast.com/mrd/methods/drivers/
  // Fetch all data as json.
  ulrLocalhost: string = URL_LOCALHOST;

  constructor( private _http: Http, private _requestService: RequestService ) {}

  getSelectYearDrivers(val: any){
      return this._requestService.getRequest(this.ulrLocalhost + val + DRIVERS_URLS.ALL_DRIVERS_SELECT);
  }

  show(driverId){
    return this._requestService.getRequest(this.ulrLocalhost + DRIVERS_URLS.SELECT_DRIVER_BEFORE_ID + driverId + ".json");
  }

}
