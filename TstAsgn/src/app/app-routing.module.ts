import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DriverListComponent } from './driver-list/driver-list.component';
import { DriverComponent } from './driver/driver.component';

const routes: Routes = [
  { path: '', component: DriverListComponent },
  { path: 'drivers/:id', component: DriverComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingComponents = [DriverListComponent, DriverComponent]
