export const URL_LOCALHOST = "http://ergast.com/api/f1/";

export const DRIVERS_URLS =
{
	ALL_DRIVERS_SELECT: "/drivers.json",
	SELECT_DRIVER_BEFORE_ID: "drivers/"
}

export const SEASONS_URLS =
{
	ALL_SEASONS_URL: "seasons.json?limit=100"
}
